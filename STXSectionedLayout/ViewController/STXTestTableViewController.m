//
//  STXTestTableViewController.m
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import "STXTestTableViewController.h"

@interface STXTestTableViewController ()

@end

@implementation STXTestTableViewController {
    NSUInteger _nbItem;
}

- (instancetype)initWithNumberOfItem:(NSUInteger)numberOfItem {
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        _nbItem = numberOfItem;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _nbItem;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"Item - %zd", indexPath.row];
    
    return cell;
}

- (UIScrollView *)contentView {
    return self.tableView;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
}

@end
