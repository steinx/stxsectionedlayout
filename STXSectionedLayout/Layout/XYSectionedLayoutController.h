//
//  XYSectionedLayoutController.h
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol XYContentScrollable;

NS_ASSUME_NONNULL_BEGIN

@interface XYSectionedLayoutController : NSObject

@property (nonatomic, weak) UIView *sectionHeaderView;
@property (nonatomic, weak) UIScrollView *scrollableContentContainer;

- (instancetype)initWithContainer:(UIScrollView *)containerView;

- (void)moveToContentScrollable:(id<XYContentScrollable>)contentScrollable;

@end

NS_ASSUME_NONNULL_END
