//
//  XYSectionedLayoutController.m
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import "XYSectionedLayoutController.h"
#import "XYContentScrollable.h"
#import "XYScrollDelegateProxy.h"
#import "UIScrollView+GestureResolver.h"

typedef NSMapTable<UIScrollView *, XYScrollDelegateProxy *> XYScrollProxyMap;
typedef NSMapTable<UIScrollView *, NSValue *> XYScrollableOffsetMap;

@interface XYSectionedLayoutController () <UIScrollViewDelegate>

@property (nonatomic, weak) UIScrollView *containerView;

@property (nonatomic, weak) id<XYContentScrollable> currentScrollable;

@property (nonatomic, readonly) CGFloat sectionHeaderTopOffset;
@property (nonatomic, readonly) UIScrollView *currentScollableContentView;

@property (nonatomic, readonly) CGPoint containerScrollCheckPoint;
@property (nonatomic, readonly) CGPoint contentScrollableCheckPoint;

@property (nonatomic, assign, getter=isSectionHeaderOnTop) BOOL sectionHeaderOnTop;
@property (nonatomic, assign) BOOL canContainerScroll;
@property (nonatomic, assign) BOOL canCurrentScrollableScroll;

@property (nonatomic, strong) XYScrollableOffsetMap *offsetMap;

@property (nonatomic, strong) XYScrollDelegateProxy *containerDelegateProxy;
@property (nonatomic, strong) XYScrollDelegateProxy *scrollableContainerDelegateProxy;
@property (nonatomic, strong) XYScrollProxyMap *scrollableDelegateProxyMap;

@end

@implementation XYSectionedLayoutController {
    BOOL _isScrollableAtRecoveredCheckPointOffset;
}

- (instancetype)initWithContainer:(UIScrollView *)containerView {
    if (self = [super init]) {
        self.containerView = containerView;
        
        _canContainerScroll = YES;
        _sectionHeaderOnTop = NO;
        _canCurrentScrollableScroll = NO;
        _isScrollableAtRecoveredCheckPointOffset = NO;
    }
    return self;
}

- (void)moveToContentScrollable:(id<XYContentScrollable>)contentScrollable {
    self.currentScrollable = contentScrollable;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.containerView]) {
        __auto_type offsetY = scrollView.contentOffset.y;
        
        if (offsetY >= self.sectionHeaderTopOffset) {
            scrollView.contentOffset = self.containerScrollCheckPoint;
            self.sectionHeaderOnTop = YES;
        } else {
            self.sectionHeaderOnTop = NO;
        }
    } else if ([scrollView isEqual:self.currentScollableContentView]) {
        BOOL needsToPurgeOffset = YES;
        
        __auto_type scrollableCheckPoint = self.contentScrollableCheckPoint;
        
        if (!self.canCurrentScrollableScroll) {
            scrollView.contentOffset = scrollableCheckPoint;
            needsToPurgeOffset = NO;
        }
        
        __auto_type offsetY = scrollView.contentOffset.y;
        BOOL scrollableReachsTop = offsetY <= 0;
        
        if (scrollableReachsTop) {
            self.canCurrentScrollableScroll = NO;
            scrollView.contentOffset = scrollableCheckPoint;
            needsToPurgeOffset = NO;
        }
        _isScrollableAtRecoveredCheckPointOffset = !CGPointEqualToPoint(scrollableCheckPoint, CGPointZero)
        && CGPointEqualToPoint(scrollView.contentOffset, scrollableCheckPoint);
        
        self.canContainerScroll = scrollableReachsTop;
        
        if (needsToPurgeOffset) {
            [self purgeSavedScrollableOffset];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView != self.scrollableContentContainer) { return; }
    
    self.containerView.scrollEnabled = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView != self.scrollableContentContainer) { return; }
    
    self.containerView.scrollEnabled = YES;
}

#pragma mark - Private
- (XYScrollDelegateProxy *)proxyingDelegationForScrollViewIfNeeded:(UIScrollView *)scrollView {
    if (scrollView.delegate == self) {
        return nil;
    }
    
    if ([scrollView.delegate isKindOfClass:[XYScrollDelegateProxy class]]) {
        return (XYScrollDelegateProxy *)scrollView.delegate;
    }
    
    if (scrollView.delegate) {
        __auto_type proxy = [[XYScrollDelegateProxy alloc] initWithDelegator:scrollView.delegate
                                                                 interceptor:self];
        scrollView.delegate = proxy;
        
        return proxy;
    } else {
        scrollView.delegate = self;
    }
    
    return nil;
}

- (void)purgeSavedScrollableOffset {
    [self.offsetMap removeObjectForKey:self.currentScollableContentView];
}

#pragma mark - Accessor
- (void)setContainerView:(UIScrollView *)containerView {
    _containerView = containerView;
    _containerDelegateProxy = [self proxyingDelegationForScrollViewIfNeeded:_containerView];
    
    __weak typeof(self) weakself = self;
    _containerView.resolver = ^BOOL(UIGestureRecognizer *gestureRecognizer, UIGestureRecognizer *otherGestureRecognizer) {
        __strong typeof(self) strongself = weakself;
        
        CGFloat segmentedPageHeight = CGRectGetHeight(strongself->_containerView.frame) - CGRectGetHeight(strongself.sectionHeaderView.frame);
        CGPoint currentPoint = [gestureRecognizer locationInView:strongself->_containerView];
        
        BOOL isTouchInsideSegmentedPage = CGRectContainsPoint(
                                                              CGRectMake(0, strongself->_containerView.contentSize.height - segmentedPageHeight, CGRectGetWidth(strongself->_containerView.frame), segmentedPageHeight),
                                                              currentPoint
                                                              );
        if (isTouchInsideSegmentedPage) {
            return YES;
        }
        
        return NO;
    };
}

- (void)setCurrentScrollable:(id<XYContentScrollable>)currentScrollable {
    if (_currentScrollable == currentScrollable) {
        return;
    }
    
    if (_currentScrollable) {
        __auto_type contentView = _currentScrollable.contentView;
        __auto_type scrollableOffset = contentView.contentOffset;
        if (!CGPointEqualToPoint(scrollableOffset, CGPointZero)) {
            // Store the offset to map, so that the offset can be recovered at the right moment
            [self.offsetMap setObject:[NSValue valueWithCGPoint:scrollableOffset] forKey:contentView];
        }
    }
    
    _currentScrollable = currentScrollable;
    
    __auto_type contentView = _currentScrollable.contentView;
    
    BOOL shouldProxying = ![self.scrollableDelegateProxyMap objectForKey:contentView] && contentView.delegate != self;
    if (shouldProxying) {
        __auto_type scrollProxy = [self proxyingDelegationForScrollViewIfNeeded:contentView];
        if (scrollProxy) {
            [self.scrollableDelegateProxyMap setObject:scrollProxy forKey:contentView];
        }
    }
}

- (void)setSectionHeaderOnTop:(BOOL)sectionHeaderOnTop {
    _sectionHeaderOnTop = sectionHeaderOnTop;
    
    if (_sectionHeaderOnTop) {
        self.canCurrentScrollableScroll = YES;
        self.canContainerScroll = NO;
    } else {
        if (!self.canContainerScroll && !_isScrollableAtRecoveredCheckPointOffset) {
            self.containerView.contentOffset = self.containerScrollCheckPoint;
        } else {
            self.canCurrentScrollableScroll = NO;
        }
    }
}

- (void)setScrollableContentContainer:(UIScrollView *)scrollableContentContainer {
    _scrollableContentContainer = scrollableContentContainer;
    _scrollableContainerDelegateProxy = [self proxyingDelegationForScrollViewIfNeeded:_scrollableContentContainer];
}

- (XYScrollProxyMap *)scrollableDelegateProxyMap {
    if (!_scrollableDelegateProxyMap) {
        _scrollableDelegateProxyMap = [XYScrollProxyMap weakToStrongObjectsMapTable];
    }
    return _scrollableDelegateProxyMap;
}

- (XYScrollableOffsetMap *)offsetMap {
    if (!_offsetMap) {
        _offsetMap = [XYScrollableOffsetMap weakToStrongObjectsMapTable];
    }
    return _offsetMap;
}

- (CGFloat)sectionHeaderTopOffset {
    return CGRectGetMinY(self.sectionHeaderView.frame);
}

- (CGPoint)containerScrollCheckPoint {
    return CGPointMake(0, self.sectionHeaderTopOffset);
}

- (CGPoint)contentScrollableCheckPoint {
    __auto_type rawCheckPoint = [self.offsetMap objectForKey:self.currentScollableContentView];
    __auto_type checkPoint = CGPointZero;
    
    if (rawCheckPoint) {
        checkPoint = [rawCheckPoint CGPointValue];
    }
    
    return checkPoint;
}

- (UIScrollView *)currentScollableContentView {
    return self.currentScrollable.contentView;
}

@end
