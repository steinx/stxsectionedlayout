//
//  XYScrollDelegateProxy.m
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import "XYScrollDelegateProxy.h"

@implementation XYScrollDelegateProxy {
    __weak id<UIScrollViewDelegate> _delegator;
    __weak id<UIScrollViewDelegate> _interceptor;
    
    struct {
        unsigned int didScroll:1;
        unsigned int willBeginDragging:1;
        unsigned int didEndDragging:1;
    } _delegatorImpFlags;
    
    struct {
        unsigned int didScroll:1;
        unsigned int willBeginDragging:1;
        unsigned int didEndDragging:1;
    } _interceptorImpFlags;
}

- (instancetype)initWithDelegator:(id<UIScrollViewDelegate>)delegator
                      interceptor:(id<UIScrollViewDelegate>)interceptor
{
    if (self) {
        _delegator = delegator;
        _interceptor = interceptor;
        
        __auto_type didScrollSel = @selector(scrollViewDidScroll:);
        _delegatorImpFlags.didScroll = [delegator respondsToSelector:didScrollSel];
        _interceptorImpFlags.didScroll = [interceptor respondsToSelector:didScrollSel];
        
        __auto_type willBeginDraggingSEL = @selector(scrollViewWillBeginDragging:);
        _delegatorImpFlags.willBeginDragging = [delegator respondsToSelector:willBeginDraggingSEL];
        _interceptorImpFlags.willBeginDragging = [interceptor respondsToSelector:willBeginDraggingSEL];
        
        __auto_type didEndDragging = @selector(scrollViewDidEndDragging:willDecelerate:);
        _delegatorImpFlags.didEndDragging = [delegator respondsToSelector:didEndDragging];
        _interceptorImpFlags.didEndDragging = [interceptor respondsToSelector:didEndDragging];
    }
    
    return self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_interceptorImpFlags.didScroll) {
        [_interceptor scrollViewDidScroll:scrollView];
    }
    
    if (_delegatorImpFlags.didScroll) {
        [_delegator scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (_interceptorImpFlags.willBeginDragging) {
        [_interceptor scrollViewWillBeginDragging:scrollView];
    }
    
    if (_delegatorImpFlags.willBeginDragging) {
        [_delegator scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (_interceptorImpFlags.didEndDragging) {
        [_interceptor scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
    
    if (_delegatorImpFlags.didEndDragging) {
        [_delegator scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    if ([_interceptor respondsToSelector:aSelector]) {
        return _interceptor;
    }
    
    if ([_delegator respondsToSelector:aSelector]) {
        return _delegator;
    }
    
    return nil;
}

- (BOOL)respondsToSelector:(SEL)aSelector {
    return aSelector == @selector(scrollViewDidScroll:) ||
    [_interceptor respondsToSelector:aSelector] ||
    [_delegator respondsToSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)invocation {
    void *nullPointer = NULL;
    [invocation setReturnValue:&nullPointer];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector {
    return [NSObject instanceMethodSignatureForSelector:@selector(init)];
}

@end
