//
//  UIScrollView+GestureResolver.h
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef BOOL(^XYGestureRecognizerResolver)(UIGestureRecognizer *gestureRecognizer, UIGestureRecognizer *otherGestureRecognizer);

@interface UIScrollView (GestureResolver)

@property (nonatomic, copy) XYGestureRecognizerResolver resolver;

@end
