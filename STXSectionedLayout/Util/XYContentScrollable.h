//
//  XYContentScrollable.h
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

@import UIKit;

@protocol XYContentScrollable <NSObject>

@property (nonatomic, readonly) UIScrollView *contentView;

@end
