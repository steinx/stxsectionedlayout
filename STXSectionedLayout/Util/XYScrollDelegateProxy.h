//
//  XYScrollDelegateProxy.h
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYScrollDelegateProxy : NSProxy <UIScrollViewDelegate>

- (instancetype)initWithDelegator:(id<UIScrollViewDelegate>)delegator
                      interceptor:(id<UIScrollViewDelegate>)interceptor;

@end
