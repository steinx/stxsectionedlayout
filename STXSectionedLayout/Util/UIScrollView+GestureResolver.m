//
//  UIScrollView+GestureResolver.m
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import "UIScrollView+GestureResolver.h"
#import <objc/runtime.h>

@implementation UIScrollView (GestureResolver)

- (XYGestureRecognizerResolver)resolver {
    return objc_getAssociatedObject(self, @selector(resolver));
}

- (void)setResolver:(XYGestureRecognizerResolver)resolver {
    objc_setAssociatedObject(self, @selector(resolver), resolver, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    __auto_type resolver = self.resolver;
    if (!resolver) {
        return NO;
    }
    
    return resolver(gestureRecognizer, otherGestureRecognizer);
}

@end
