//
//  ViewController.m
//  STXSectionedLayout
//
//  Created by Yiming XIA on 2018/8/4.
//  Copyright © 2018年 STXProd. All rights reserved.
//

#import "ViewController.h"
#import "XYSectionedLayoutController.h"
#import "STXTestTableViewController.h"
#import <ReactiveObjC/ReactiveObjC.h>

@interface ViewController ()

@property (nonatomic, strong) XYSectionedLayoutController *layout;

@property (nonatomic, strong) NSArray<UITableViewController<XYContentScrollable> *> *vcs;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __auto_type scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    _layout = [[XYSectionedLayoutController alloc] initWithContainer:scrollView];
    
    // View blocks
    __auto_type maxWidth = CGRectGetWidth(self.view.bounds);
    __auto_type view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, maxWidth, 500)];
    view1.backgroundColor = UIColor.brownColor;
    [scrollView addSubview:view1];
    
    __auto_type view2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(view1.frame), maxWidth, 100)];
    view2.backgroundColor = UIColor.darkGrayColor;
    [scrollView addSubview:view2];
    
    // Section View
    __auto_type sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(view2.frame), maxWidth, 60)];
    sectionView.backgroundColor = UIColor.purpleColor;
    _layout.sectionHeaderView = sectionView;
    [scrollView addSubview:sectionView];
    
    __auto_type paginatedScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(sectionView.frame), maxWidth, CGRectGetHeight(self.view.bounds) - CGRectGetHeight(sectionView.frame))];
    paginatedScrollView.pagingEnabled = YES;
    _layout.scrollableContentContainer = paginatedScrollView;
    [scrollView addSubview:paginatedScrollView];
    
    __auto_type __vcs = [NSMutableArray<UITableViewController<XYContentScrollable> *> arrayWithCapacity:3];
    
    __auto_type tableVC = [[STXTestTableViewController alloc] initWithNumberOfItem:200];
    tableVC.view.frame = CGRectMake(0, 0, CGRectGetWidth(paginatedScrollView.frame), CGRectGetHeight(paginatedScrollView.frame));
    [__vcs addObject:tableVC];
    [paginatedScrollView addSubview:tableVC.view];
    
    tableVC = [[STXTestTableViewController alloc] initWithNumberOfItem:100];
    tableVC.view.frame = CGRectMake(CGRectGetMaxX(__vcs[0].view.frame), 0, CGRectGetWidth(paginatedScrollView.frame), CGRectGetHeight(paginatedScrollView.frame));
    [__vcs addObject:tableVC];
    [paginatedScrollView addSubview:tableVC.view];
    
    tableVC = [[STXTestTableViewController alloc] initWithNumberOfItem:50];
    tableVC.view.frame = CGRectMake(CGRectGetMaxX(__vcs[1].view.frame), 0, CGRectGetWidth(paginatedScrollView.frame), CGRectGetHeight(paginatedScrollView.frame));
    [__vcs addObject:tableVC];
    [paginatedScrollView addSubview:tableVC.view];
    
    self.vcs = __vcs;
    
    paginatedScrollView.contentSize = CGSizeMake(3 * CGRectGetWidth(paginatedScrollView.frame), CGRectGetHeight(paginatedScrollView.frame));
    scrollView.contentSize = CGSizeMake(maxWidth, CGRectGetMaxY(paginatedScrollView.frame));
    
    @weakify(self)
    [RACObserve(paginatedScrollView, contentOffset) subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        __auto_type offsetX = [x CGPointValue].x;
        NSInteger index = (NSInteger)roundf(offsetX / maxWidth);
        index = MIN(MAX(0, index), self.vcs.count - 1);
        
        [self.layout moveToContentScrollable:self.vcs[index]];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
